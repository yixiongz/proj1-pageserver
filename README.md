# Proj1-pageserver #

-author: Eric Zhou

-contact address email: yixiongz@uoregon.edu

-description: A local pageserver, send the content of the request page if it exist in local,
              otherwise, send the relevant status.
              - 404, if page not found
              - 403, if the name of the page is illegal
