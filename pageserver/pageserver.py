"""
  A trivial web server in Python.

  Based largely on https://docs.python.org/3.4/howto/sockets.html
  This trivial implementation is not robust:  We have omitted decent
  error handling and many other things to keep the illustration as simple
  as possible.

"""

import config    # Configure from .ini files and command line
import logging   # Better than print statements
import os
logging.basicConfig(format='%(levelname)s:%(message)s',
                    level=logging.INFO)
log = logging.getLogger(__name__)
# Logging level may be overridden by configuration 
DOCROOT = "."
import socket    # Basic TCP/IP communication on the internet
import _thread   # Response computation runs concurrently with main program


def listen(portnum):
    """
    Create and listen to a server socket.
    Args:
       portnum: Integer in range 1024-65535; temporary use ports
           should be in range 49152-65535.
    Returns:
       A server socket, unless connection fails (e.g., because
       the port is already in use).
    """
    # Internet, streaming socket
    serversocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    # Bind to port and make accessible from anywhere that has our IP address
    # socket is reachable by any address the machine happens to have
    serversocket.bind(('', portnum))  
    serversocket.listen(1)    # A real server would have multiple listeners
    # the argument to listen tells the socket library that we want it to queue up 
    # as many as 5 connect requests (the normal max) before refusing outside connections.
    return serversocket


def serve(sock, func):
    """
    Respond to connections on sock.
    Args:
       sock:  A server socket, already listening on some port.
       func:  a function that takes a client socket and does something with it
    Returns: nothing
    Effects:
        For each connection, func is called on a client socket connected
        to the connected client, running concurrently in its own thread.
    """
    while True:
        log.info("Attempting to accept a connection on {}".format(sock))
        (clientsocket, address) = sock.accept()
        _thread.start_new_thread(func, (clientsocket,))


##
# Starter version only serves cat pictures. In fact, only a
# particular cat picture.  This one.
##


# HTTP response codes, as the strings we will actually send.
# See:  https://en.wikipedia.org/wiki/List_of_HTTP_status_codes
# or    http://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html
##
STATUS_OK = "HTTP/1.0 200 OK\n\n"
STATUS_FORBIDDEN = "HTTP/1.0 403 Forbidden\n\n"
STATUS_NOT_FOUND = "HTTP/1.0 404 Not Found\n\n"
STATUS_NOT_IMPLEMENTED = "HTTP/1.0 401 Not Implemented\n\n"


def respond(sock):
    """
    This server responds only to GET requests (not PUT, POST, or UPDATE).
    """
    sent = 0
    request = sock.recv(1024)  # We accept only short requests
    request = str(request, encoding='utf-8', errors='strict')
    log.info("--- Received request ----")
    log.info("Request was {}\n***\n".format(request))

    parts = request.split()                   # get the relative path of the target page
    if len(parts) > 1 and parts[0] == "GET":  # check "GET"
      path = parts[1].split("/")              # split to get the full name of the target page
      if path[-1][0] == "~" or path[-1][0:2]== ".." or parts[1].find("//") != -1:
        # check if the page's name is illegal(~ // ..)
        # path[-1] is the full name of the page
          # both path[-1][0] and path[-1][0:2] are the beginning checking(~ ..)
          # parts[1].find() is double slash checking(//)
        transmit(STATUS_FORBIDDEN, sock)  # if illegal, send STATUS_FORBIDDEN
      else:
        # if legal                               
        if path[-1][-5:] == ".html" or path[-1][-4:] == ".css":
        # ending of page's name checking
          source_path = os.path.join(DOCROOT, parts[1][1:])
          # if legal, find the absolute path of the target page by using DOCROOT(combining)
          transmit(STATUS_OK, sock)
          # send STATUS_OK
          try: 
            with open(source_path, 'r', encoding='utf-8') as source:
              for line in source:
                transmit(line.strip(), sock)
          # try to open the target page within specific path
          # if exist, send content to server
          except OSError as error:
            transmit(STATUS_NOT_FOUND, sock)
          # if not exist, send STATUS_NOT_FOUND as an error

          ##
          # reference:  spew.py
          ##
          
    else:
        log.info("Unhandled request: {}".format(request))
        transmit(STATUS_NOT_IMPLEMENTED, sock)
        transmit("\nI don't handle this request: {}\n".format(request), sock)

    sock.shutdown(socket.SHUT_RDWR)
    sock.close()
    return


def transmit(msg, sock):
    """It might take several sends to get the whole message out"""
    sent = 0
    while sent < len(msg):
        buff = bytes(msg[sent:], encoding="utf-8")
        sent += sock.send(buff)

###
#
# Run from command line
#
###


def get_options():
    """
    Options from command line or configuration file.
    Returns namespace object with option value for port
    """
    # Defaults from configuration files;
    #   on conflict, the last value read has precedence
    options = config.configuration()
    # We want: PORT, DOCROOT, possibly LOGGING

    if options.PORT <= 1000:
        log.warning(("Port {} selected. " +
                         " Ports 0..1000 are reserved \n" +
                         "by the operating system").format(options.port))

    return options


def main():
    global DOCROOT  # setup DOCROOT
    options = get_options()
    DOCROOT = options.DOCROOT
    port = options.PORT
    if options.DEBUG:
        log.setLevel(logging.DEBUG)
    sock = listen(port)
    log.info("Listening on port {}".format(port))
    log.info("Socket is {}".format(sock))
    serve(sock, respond)


if __name__ == "__main__":
    main()
